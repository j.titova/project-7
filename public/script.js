function click1() {
  let f1 = document.getElementsByName("field1");
  let f2 = document.getElementsByName("field2");
  f1[0].value = f1[0].value.replace(/[^\d+$]/g, '');
  f2[0].value = f2[0].value.replace(/[^\d+$]/g, '');
  let r = document.getElementById("result");
  r.innerHTML = parseInt(f1[0].value) * parseInt(f2[0].value);
  return false;
}

function itemChange(){
  document.getElementById('chosenItem').innerHTML = document.getElementById(document.getElementById('item').value).innerHTML

  let r = document.getElementById("price");
  if (document.getElementById("item").value == "item1"){
    r.innerHTML = 70;
  } else r.innerHTML = 0;
}

function click2(){
  let r = document.getElementById('price');
  let f1 = document.getElementById('item').value;
  if (f1 == 'item2'){
    if (document.getElementById('fries1').checked) r.innerHTML = 60;
    else if (document.getElementById('fries2').checked) r.innerHTML = 75;
    else if (document.getElementById('fries3').checked) r.innerHTML = 90;
  } else if (f1 == 'item3'){
    if (document.getElementById('meat').checked) r.innerHTML = 200;
    else if (document.getElementById('noMeat').checked) r.innerHTML = 150;
    if (document.getElementById('cheese').checked) r.innerHTML = parseInt(r.innerHTML) + 20;
  }
}

function click3(){
  let f1 = document.getElementById('price').value;
  let f2 = document.getElementsByName('amount');
  f2[0].value = f2[0].value.replace(/[^\d+$]/g, '');
  document.getElementById('total').innerHTML = parseInt(f1) * parseInt(f2[0].value);
  return false;
}

window.addEventListener("DOMContentLoaded", function(){
  $('.carousel').slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
        slidesToShow: 3,
        }
      },
      {
        breakpoint: 769,
        settings: {
        slidesToShow: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
        slidesToShow: 2,
        }
      },
      {
        breakpoint: 330,
        settings: {
        slidesToShow: 1,
        }
      }

    ]
  });
});
